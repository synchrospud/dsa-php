<?php declare(strict_types=1);

namespace LinkedList;

use PHPUnit\Framework\TestCase;

final class LinkedListTest extends TestCase
{

	private function setUpList() {
		$linked_list = new LinkedList();		
		$linked_list->add("a");
		$linked_list->add("b");
		$linked_list->add("c");
		$linked_list->add("d");	

		return $linked_list;
	}

	public function testInstantiateLinkedList(): void {
		$this->assertInstanceOf(
			LinkedList::class,
			new LinkedList()
		);
	}

	public function testAddNode(): void {
		$linked_list = new LinkedList();
		$linked_list->add("a");
		$this->assertEquals(
			1,
			$linked_list->getSize(), 
		);

		$this->assertEquals(
			"a",
			$linked_list->getHead()->value
		);

		$linked_list->add("b");
		$linked_list->add("c");
		$linked_list->add("d");		
		$this->assertEquals(
			4,
			$linked_list->getSize(),
		);

		$this->assertEquals(
			"a",
			$linked_list->getHead()->value,
		);	

		$linked_list->add("pre a", 0);	
		$this->assertEquals(
			"pre a",
			$linked_list->getHead()->value
		);

		$linked_list->add("pre d", 4);

		$this->assertEquals(
			"pre d",
			$linked_list->get(4)->value
		);

		$this->assertEquals(
			"d",
			$linked_list->get(5)->value
		);	

		$this->assertEquals(
			"c",
			$linked_list->get(3)->value
		);	

		$linked_list->add("pre b", 2);

		$this->assertEquals(
			"pre b",
			$linked_list->get(2)->value
		);		
	}

	public function testGetException() : void {
		$this->expectException(\Exception::class);				
		$linked_list = new LinkedList();		
		$linked_list->get(4);
	}

	public function testGetNode() : void {
		$linked_list = $this->setUpList();

		$this->assertEquals(
			$linked_list->getHead()->value,
			$linked_list->get(0)->value
		);

		$this->assertEquals(
			"d",
			$linked_list->get($linked_list->getSize()-1)->value
		);
	}

	public function testList() :void {
		$linked_list = $this->setUpList();

		$expected = "a b c d";

		$this->assertEquals(
			$expected,
			$linked_list->toString()
		);
	}

	public function testReverse() : void {
		$linked_list = new LinkedList();
		$this->assertEquals(
			$linked_list->reverse(),
			null
		);

		$linked_list->add("a");
		$linked_list->add("b");
		$linked_list->reverse();
		$this->assertEquals(
			"b a",
			$linked_list->toString()
		);

		$linked_list = $this->setUpList();
		$linked_list->reverse();

		$this->assertEquals(
			"d",
			$linked_list->get(0)->value
		);

		$this->assertEquals(
			"c",
			$linked_list->get(1)->value
		);

		$this->assertEquals(
			"b",
			$linked_list->get(2)->value
		);

		$this->assertEquals(
			"a",
			$linked_list->get(3)->value
		);	

		$expected = "d c b a";

		$this->assertEquals(
			$expected,
			$linked_list->toString()
		);							
	}

	public function testSort() : void {
		$linked_list = $this->setUpList();
		$linked_list->reverse();
		$linked_list->sort();
		$this->assertEquals(
			"a b c d",
			$linked_list->toString()
		);

		$linked_list = new LinkedList();
		$linked_list->add("Zebra");
		$linked_list->add("Croissant");
		$linked_list->add("Animal");
		$linked_list->add("Partridge");
		$linked_list->add("Balloon");
		$linked_list->add("Coffee");
		$linked_list->add("Helmet");
		$linked_list->add("Road");

		$linked_list->sort();
		$expected = "Animal Balloon Coffee Croissant Helmet Partridge Road Zebra";

		$this->assertEquals(
			$expected,
			$linked_list->toString()
		);

	}
}

?>
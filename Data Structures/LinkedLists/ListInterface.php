<?php

namespace LinkedList;

use LinkedList\Node;

interface ListInterface 
{

	public function getHead() : Node;
	public function getSize() : int;
    public function add($value, int $index) : void;
    public function get(int $index) : Node;    
    public function delete(int $index) : bool;
    public function toString() : string;
    public function reverse() : void;
    public function sort() : void;    

}

?>
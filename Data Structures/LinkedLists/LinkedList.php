<?php

namespace LinkedList;

use LinkedList\Node;

class LinkedList implements ListInterface
{

	private $head;
	private $size;

	public function __construct(){
		$this->head = null;
		$this->size = 0;
	}

	public function getSize() : int {
		return $this->size;
	}

	public function getHead() : Node {
		return $this->head;
	}

    public function add($value, int $index = NULL) : void {
    	$curr_node = $this->head;    	
    	if(!$this->head){
    		$this->head = new Node($value);
    	} 
    	elseif($index == 0 && !is_null($index)){
    		$this->head = new Node($value, $this->head);
    	} elseif(is_null($index)) {
    		while($curr_node->next){
    			$curr_node = $curr_node->next;
    		}
    		$curr_node->next = new Node($value);
    	} else {
    		for($i = 1; $i < $index; $i++){
    			$curr_node = $curr_node->next;
    		}
    		$old_next = $curr_node->next;
    		$curr_node->next = new Node($value, $old_next);
    	}
    	
    	$this->size++;    	
    }
    
    public function get(int $index) : Node {
    	if(!$this->head || $index >= $this->size){
    		throw new \Exception("Index out of bounds");
    	}
    	$i = 0;
    	$rtn_node = $this->head;

    	for($i = 1; $i <= $index; $i++){
    		$rtn_node = $rtn_node->next;    		
    	}

    	return $rtn_node;
    }  
    
    public function delete(int $index) : bool{
        if(!$this->head || $index >= $this->size){
            throw new \Exception("Index out of bounds");
        }        
    }
    
    public function toString() : string {
    	$curr_node = $this->head;
        $rtnVal = "";
        if(!$this->head->next){
           return $this->head->value;
        } else {
            while($curr_node){
                $rtnVal .= $curr_node->value . " ";
                $curr_node = $curr_node->next;
            }
        }

        return trim($rtnVal);
    }
    
    public function reverse() : void {
        if($this->head && $this->head->next){
            $curr_node = $this->head;
            $new = null;
             while($curr_node){
                $temp = $curr_node->next;
                $curr_node->next = $new;
                $new = $curr_node;
                $curr_node = $temp;
            }
            $this->head = $new;
        }
    }
    
    #Maybe change this in future to something that isn't lazy with terrible time complexity. 
    public function sort() : void {
        $curr_node = $this->head;   
        $next = null;  
          
        if(!$this->head == null) {  
            while($curr_node != null) {  
                $next = $curr_node->next;  
                while($next != null) {  
                    if($curr_node->value > $next->value) {  
                        $temp = $curr_node->value;  
                        $curr_node->value = $next->value;  
                        $next->value = $temp;  
                    }  
                    $next = $next->next;  
                }  
                $curr_node = $curr_node->next;  
            }      
        } 
    }

}

?>